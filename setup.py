import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

with open('requirements.txt') as file:
    required = file.read().splitlines()

setup(
    name="temporary_tables",
    version="0.2.2-b3",
    author="InkaLabs",
    author_email="inkalabs@inka-labs.com",
    description=("Super usefull tool for create temporary tables"),
    license="Freeware",
    keywords="temporary_tables",
    url="https://bitbucket.org/inkalabsinc/temporary-tables",
    packages=['temporary_tables'],
    long_description=read('README.md'),
    install_requires=required,
    classifiers=[
        "Development Status :: 0 - Beta",
        "Natural Language :: English",
        "Programming Language :: Python :: 2.7",
        "Topic :: Utilities",
        "License :: Freeware",
    ],
)
