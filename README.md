Make sure to use the correct version of the package following your django version:

For Django <= 1.9 use 0.1.1-b0

For Django >= 1.10 use 0.2.1-b0

Usefull tool for creating Temporary table, 
in order to use it, just install the package, 
then you can use it.
 
    with TemporaryTable(table_names=(
            Django_class._meta.db_table,),
            logger=logger) as tmp_table:
        new_model = create_model_clone(
            prefix, Model_django)

perform operations with the new returned model