"""
Tool to import new data from an external file into a temporary table.

Then rename the table as the orignal table, and drop the original.
"""

import logging
import copy

from django.db import connection, models
from django.db.utils import DatabaseError
from warnings import filterwarnings

try:
    import MySQLdb
    mysql_warning = MySQLdb.Warning
    filterwarnings('ignore', category=mysql_warning)
except:
    mysql_warning = None

try:
    import psycopg2
    postgresql_warning = psycopg2.Warning
except:
    postgresql_warning = None


logger = logging.getLogger(__name__)


def create_model_clone(original_model):
    """Create a new Django Model in RAM memory cloning an original one."""
    class Meta:
        db_table = "new_%s" % (original_model._meta.db_table)
        app_label = original_model._meta.app_label
    attrs = {
        'Meta': Meta,
        '__module__': original_model.__module__,
    }
    for field in original_model._meta.fields:
        attrs[field.name] = copy.copy(field)
        if isinstance(field, models.ForeignKey):
            rel = copy.copy(field.rel_class)
            rel.related_name = '_new_' + field.related_query_name()
            attrs[field.name].rel_class = rel

    return type("new%s" % (
        original_model.__name__), (models.Model, ), attrs)


class TemporaryTable(object):

    """
    Temporary table object.

    This is a new table, cloned from an original one, this table would then
    be used to store the new data, and when an import or other heavy operation
    is done, this would be promoted as the new main non-temporary table
    """

    DATABASE_SENTENCES = {
        'mysql': {
            'error_handler': mysql_warning,
            'drop_table': 'DROP TABLE IF EXISTS {table};',
            'create_table': 'CREATE TABLE IF NOT EXISTS {table_new} LIKE '
                            '{table_old};',
            'rename_table': "rename table {from_name} to {to_name};"
        },
        'postgresql': {
            'error_handler': postgresql_warning,
            'drop_table': 'DROP TABLE IF EXISTS {table} CASCADE;',
            'drop_sequence': (
                'DROP SEQUENCE IF EXISTS {table_name}_seq CASCADE'),
            'create_table': (
                "CREATE TABLE IF NOT EXISTS {table_new} (LIKE "
                "{table_old} INCLUDING INDEXES);CREATE SEQUENCE"
                " {table_new}_seq; ALTER TABLE {table_new} ALTER id "
                "SET DEFAULT NEXTVAL('{table_new}_seq');"),
            'rename_table': 'ALTER TABLE {from_name} RENAME TO {to_name};'
        },
    }

    def __init__(self, table_names=None, logger=None, db=None):
        """
        Initialize the object.

        table_names is the set of original tables to be cloned.
        logger is the logger to be used
        and, optionally a database connection can be passed as db.
        """
        if db is None:
            self.rollback()
            self.__exit__(False, False, True)
        if table_names is None:
            table_names = []
        self.cursor = None
        self.table_names = table_names
        self.db = db
        self.back_to_previous_state = False

        if not logger:
            self.logger = logging.getLogger(__name__)
        else:
            self.logger = logger

        self.reset_cursor()
        for original_table in self.table_names:
            new_table = "new_{table}".format(table=original_table)
            self.drop_table(new_table)
            self.create_table(new_table, original_table)

    def reset_cursor(self):
        """Release the cursor and creates a new one."""
        if self.cursor:
            self.cursor.close()
        self.cursor = connection.cursor()

    def create_table(self, name, template_name):
        """Create a new database table."""
        if self.db == 'postgresql':
            self.cursor.execute(
                TemporaryTable.DATABASE_SENTENCES[
                    self.db]['drop_sequence'].format(table_name=name))
        self.cursor.execute(
            TemporaryTable.DATABASE_SENTENCES[self.db]['create_table'].format(
                table_new=name, table_old=template_name))

    def rename_table(self, from_name, to_name):
        """Rename a database table."""
        self.cursor.execute(
            TemporaryTable.DATABASE_SENTENCES[self.db]['rename_table'].format(
                from_name=from_name, to_name=to_name)
        )

    def drop_table(self, table_name):
        """Drop a databaes table."""
        try:
            self.cursor.execute(
                TemporaryTable.DATABASE_SENTENCES[
                    self.db]['drop_table'].format(
                        table=table_name))
        except TemporaryTable.DATABASE_SENTENCES[self.db]['error_handler']:
            pass

    def abort_temporary_table(self, table_name):
        """Aborts the operation (in case of error)."""
        self.reset_cursor()
        self.table_names.remove(table_name)
        new_table = "new_{table}".format(table=table_name)
        self.drop_table(new_table)

    def rollback(self):
        """
        Rollback a the cloning when errors are found.

        Set the variable 'continue_on_failure' as False
        to indicate that the process that was using it has failed and must
        be returned to the previous state.
        :return:
        """
        self.back_to_previous_state = True

    def __enter__(self, *args, **kwargs):
        """Do nothing."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Relese the cursor and cleanup temporary tables."""
        self.reset_cursor()
        for table in self.table_names:
            try:
                if self.back_to_previous_state:
                    self.drop_table("{table}_old".format(table=table))
                    self.drop_table("new_{table}".format(table=table))
                    self.logger.warning(
                        "Something went wrong when filling new tables. "
                        "It will return to the previous state")
                else:
                    self.drop_table("{table}_old".format(table=table))
                    self.rename_table(table, "{table}_old".format(table=table))
                    self.rename_table("new_{table}".format(table=table), table)
                    self.drop_table("{table}_old".format(table=table))
            except DatabaseError as ex:
                self.logger.warning(
                    "{table} table does not exist {err}".format(
                        table=table, err=ex))
        if exc_tb:
            self.logger.warning(
                ("something went wrong when creating _new"
                 " tables for {err_type} {err}".format(
                    err_type=exc_type, err=exc_val)))
            return
        return True

